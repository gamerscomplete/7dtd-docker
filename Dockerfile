FROM debian:latest

RUN apt-get update && apt-get install -y lib32gcc1 wget

RUN wget http://media.steampowered.com/installer/steamcmd_linux.tar.gz -O /tmp/steamcmd_linux.tar.gz

RUN mkdir /opt/steamcmd

RUN cd /opt/steamcmd/ && tar -xvzf /tmp/steamcmd_linux.tar.gz

RUN mkdir /opt/7dtd

RUN /opt/steamcmd/steamcmd.sh +login anonymous +force_install_dir /opt/7dtd/ +app_update 294420 -beta latest_experimental +quit

RUN echo wotskies

RUN /opt/steamcmd/steamcmd.sh +login anonymous +force_install_dir /opt/7dtd/ +app_update 294420 -beta latest_experimental +quit

COPY startserver.sh /opt/7dtd/
RUN chmod +x /opt/7dtd/startserver.sh
COPY serverconfig.xml /opt/7dtd/

WORKDIR /opt/7dtd

#TCP
EXPOSE 8080
EXPOSE 8081
EXPOSE 8082
EXPOSE 26900

#UDP
EXPOSE 26900
EXPOSE 26901
EXPOSE 26902


CMD ["./startserver.sh", "-configfile=serverconfig.xml"]
